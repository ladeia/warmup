#ifndef DADINHOS_H
#define DADINHOS_H

class dadinhos
{
public:
    dadinhos(float centavos, int quantidade, int pessoas);
    float valor();
private:
    float centavos;
    int pessoas;
    int quantidade;
};

#endif // DADINHOS_H
