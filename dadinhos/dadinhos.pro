#-------------------------------------------------
#
# Project created by QtCreator 2014-08-29T15:49:03
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = dadinhos
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    dadinhos.cpp

HEADERS += \
    dadinhos.h
